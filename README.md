Histoires d'un ~~étudiant~~ salarié perfectionniste sous pression
================================================================

_23 octobre 2019 - AFPy Lyon_

---

Mes 7 premiers mois à la découverte de la vie professionnelle d'un développeur junior

![AFPy logo](img/afpylyon.png)
