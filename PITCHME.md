#### Histoires d'un salarié perfectionniste sous pression

![AFPy logo](img/afpylyon.png)

_23 octobre 2019 - Frédéric Zind - AFPy Lyon_

Note:

- suite AFPy 23 janvier 19
- qui y était ?

+++

Mes 7 premiers mois à la découverte de la vie professionnelle d'un dev

![ansible logo](img/ansible.png)
![bash logo](img/bash.png)
![black logo](img/black.png)
![debian logo](img/debian.png)
![django logo](img/django.png)
![gitlab logo](img/gitlab.png)

![git logo](img/git.png)
![knacss logo](img/knacss.png)
![plantuml logo](img/plantuml.png)
![pytest logo](img/pytest.png)
![python logo](img/python.png)
![virtualbox logo](img/virtualbox.png)

Note:

- Départ dynamique, lever de mains
- niveau débutant, mais beaucoup de choses
- Qui dev ? Junior ?
- Qui dev python ?
- Qui dev django ?
- Qui admin ?

---

### Où on est ?

Note:

- Qui déjà venu dans une cordée ?
- DOUBLE CLICK

+++

![La Cordée logo](img/200-lacordee.png)
@ul
- `communauté de travail != #coworking`
- du WIFI
- du café
@ulend

+++

![La Cordée logo](img/200-lacordee.png)

… mais surtout :
@ul
- du partage
- une communauté IRL / virtuelle
- des jeux de mots / des goûters (de folie !)
- brefs… des humains @fa[users]
@ulend

Note:

- depuis mars 2017
- teletravail depuis Mai 2019
- 6 à Lyon
- Annecy
- Rennes
- Nantes
- …

---

### Moi

![ti99](img/ti99.jpg)

Note:

- je m'appelle…
- technicien, mécanique
- logistique & assistance
- gestion d'équipe, clients & matériels
- libriste pratiquant depuis Debian 3 (woody/2002)
- 2018 veut devenir pro => Python

---

### Mon boulot

+++

![MF2i logo](img/200-mf2i.png)

@ul
- www.mf2i.fr
- TPE : infra & infogérance
- Arrivée en _mars 2019_
- **le seul développeur**
@ulend

---

### Avertissement

@fa[exclamation-triangle]

Ceci n'est **pas de recette miracle** : seulement un retour d'expérience avec **plus ou moins** de recul

---

### Pourquoi ?

J'ai pas de collègues dev !

+++

### Le sujet :

> Comment démarrer une activité de dév quand on est junior

+++

Ou…

> Comment (essayer) de ne pas se perdre entre :

@ul
- le code
- la documentation (si, si)
- les déploiements
- la communication (client, collègues/direction)
- ~~la maintenance~~
@ulend

Note:

- le client est vital, mais pense que l'IT c'est facile
- refacto is bad

---

### Organisation

+++

#### Choisir sa version de python

![debian logo](img/debian.png)

@fa[heart]

![python logo](img/python.png)

Note:

- de zéro il aurait été jouable de surfer

+++


#### Git

![git logo](img/git.png)

Note:

- qui ne connait pas git?
- systeme de gestion de version incontournable
- outil structurant
- seconde peau

+++

_Mon flux_ git

@ul
- historique `git` == documentation
- branches _au long court_ == re-utilisation
- une branche par fonctionnalité == concentration
@ulend

Note:

- mode d'emploi plutôt que journal
- commit atomique et testé
- aide de git & app django
- moins de perturbation extérieure
- dilemme du ticket : peu mais rempli ou plein mais vide

+++

##### Mes branches…

`master` & `staging` : _fusion seulement_

![git-branch-staging logo](img/git-branch-staging.png)

Note:

- d'environnement
- pas touche à master
- reset autorisé sur staging

+++

##### Mes branches…

@ul
- `core`, `a14n`, `user`, …
- `documentation`, `candidate`, `drover`, `sipa`, …
- `candidate-46`, `drover-56`, `sipa-50`, …
- `wip-47`, `wip-55`, …
- `old-17`, `old-35`, …
- ~~`issue33`, `issue27`, …~~
@ulend

Note:

- Django
- Fonctionnalité
- demande testable
- demande non-testable
- sauvegarde temporaire

+++

![django-apps-01 diagram](https://free_zed.gitlab.io/afpy19/diagrams/django-apps-01.png)

Note:

- exemple projet Django partiel et basique

+++

![django-apps-02 diagram](https://free_zed.gitlab.io/afpy19/diagrams/django-apps-02.png)

+++

![django-apps-03 diagram](https://free_zed.gitlab.io/afpy19/diagrams/django-apps-03.png)

+++

![django-apps-04 diagram](https://free_zed.gitlab.io/afpy19/diagrams/django-apps-04.png)

Note:

- objectif garder l'isolement du code aussi

+++

Des options qui assurent

@ul
- `git rebase --interactive <hash>^`
- `git diff --staged`
- `git add --patch`
- `git cherry-pick <hash>`
- `git commit --fixup=<hash>`
- `git rebase -i --autosquash --autostash <hash>^`
@ulend

Note:

- ? reflog
- ? stash
- ? rerere

+++

… et des alias

```init
[alias]
    ba = branch -av
    cia = commit --amend --no-edit
    ci = commit
    co = checkout
    diff = diff --ignore-all-space
    lga = log --graph --oneline --decorate --since=7days --all
    lgla = log --graph --oneline --decorate --date=short --all
    lgl = log --graph --oneline --decorate
    lg = log --graph --oneline --decorate --since=7days
    st = status
```

Note:

- pas beaucoup
- 3 inutilisés
- 2 ou 3 manquants
- message par defaut de merge ?

---

### Infra

Déploiement + Hébergement = SaaS !

+++

… mais non (-;

+++
Des machines !

![work-network-01 diagram](https://free_zed.gitlab.io/afpy19/diagrams/work-network-01.png)

+++
Des machines !

![work-network-02 diagram](https://free_zed.gitlab.io/afpy19/diagrams/work-network-02.png)

+++
Plein de machinesss !

![work-network-06 diagram](https://free_zed.gitlab.io/afpy19/diagrams/work-network-06.png)

---

### Automatisation

@fa[robot]
+
@fa[rocket]


Note:

- Code OK, Infra OK tour des outils plus automatique

+++

Des _crochets git_ côté client

```shell
#!/bin/sh
# .git/hooks/pre-commit

.venv/bin/black --check .
.venv/bin/flake8 --config=setup.cfg
.venv/bin/pylint --rcfile=setup.cfg --load-plugins pylint_django *.py
.venv/bin/pytest --quiet
```

Note:

- black : sceptique avant, convaincu maintenant
- linter : indispensable
- meilleurs conf pylint/flake8 mais pas pris le temps
- les tests c'est la vie : commit testable

+++

Des _crochets git_ côté client

```shell
#!/bin/sh
# .git/hooks/post-commit

git stash save --include-untracked
.venv/bin/pytest --cov-report term:skip-covered --cov=.
git stash pop --quiet
git log -1 HEAD
```
Note:

- ça paye pas souvent…
- rigeur dans l'isolation des commits

+++

Gitlab

![gitlab logo](img/500-gitlab.png)

Note:

- Qui connait ?

+++

![gitlab stat logo](img/gitlab-stat-mf2i.png)

Note:

- Ticket, jalon, étiquette, …
- gestion utilisateurs/groupes
- confidentialité clés/certificats/log CI
- gestion config exécuteurs/images
- ressources matérielles
- changer la cosmétique de l'instance

---

#### PlantUML

![plantuml logo](img/plantuml.png)

> Tellement facile à maintenir que vos diagrammes pourraient être à jours 10 mois après le début du projet @fa[grin-stars]

+++?code=puml/part/django-apps.iuml&title=Base

@[1-4](Nœud)
@[9-14](Paquet et fichier dans un packet)
@[29-33](Relations)

+++?code=puml/django-apps-02.puml&title=Inclusion

@[3-8](D.R.Y.)

+++?code=.gitlab-ci.yml&title=Construction via Gitlab CI

@[11-19](Magie_de_la_CI)

+++

…et voilà!

![django-apps-02 diagram](https://free_zed.gitlab.io/afpy19/diagrams/django-apps-02.png)

https://gitlab.com/free_zed/mypumlt

Note:

- pas de bitmap en historique
- style pérenne
- D.R.Y.
- implémentations pléthorique

---

#### Ansible

![ansible logo](img/500-ansible.png)

+++

![ansible long logo](img/ansible-long.png)

@ul
- automatise des commandes
- n'a pas besoin d'être installé sur la cible
- parle `YAML`, `Jinja2` & `SSH` couramment
- vérifie avant de faire
- modulaire (git, files, django, foobar, etc.)
- un niveau de configuration proche de l'infini
@ulend

+++

![ansible-network diagram](https://free_zed.gitlab.io/afpy19/diagrams/ansible-network.png)

Note:

- simplifie ce que faisaient vos scripts
- config et doc paumatoire…
- mais tellement puissant

+++

![ansible-parts-01 diagram](https://free_zed.gitlab.io/afpy19/diagrams/ansible-parts-01.png)

+++

![ansible-parts-02 diagram](https://free_zed.gitlab.io/afpy19/diagrams/ansible-parts-02.png)

+++

!["ansible-parts-03" diagram](https://free_zed.gitlab.io/afpy19/diagrams/ansible-parts-03.png)

+++

![ansible-parts-04 diagram](https://free_zed.gitlab.io/afpy19/diagrams/ansible-parts-04.png)

---

`from __future__ import ideas`

@ul
- Automatisation complète Gitlab-CI & Ansible
- Empaquetage
@ulend

Note:

- zsh-autosuggestions
- powerlevel9k
- meld
- git-push-each
- …

---

@snap[north]
### Des questions ?
@snapend

@snap[west]
![QRcode](img/qrcode-pro.zind.fr.png)
@snapend

@snap[south]
http://pro.zind.fr
@snapend

@snap[east]
### Merci !
@snapend
